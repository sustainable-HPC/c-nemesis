#!/bin/bash

#Script for running the simulations.

#Creating the directory for this simulation
rm -rf "results_wsnb_google"
#Creating the directory for this simulation
mkdir "results_wsnb_google"

#cd "results_wsnb_google"

#Creating the output directory
mkdir "results_wsnb_google/output"

#Creating the results directory
mkdir "results_wsnb_google/results"

#Code to run the simulations
clear && clear && simulationmain input/platform/homogeneousGrid5000Pstate.xml input/deploy/grid5000_deploy_wsnb_google.xml input/workload/google_2011.txt  --log=root.app:file:results_wsnb_google/output/logfile.log

cd results_wsnb_google
pwd
#Script for pre-processing the data
python3 ../scripts/extract_mig_data.py 