{
  tinypkgs ? import (fetchTarball https://gitlab.uspdigital.usp.br/sustainable-HPC/nix-packages/-/archive/master/packages-repository-ac2c0c61d9c6fae0fc64dcffb3160e8f769e21bf.tar.gz) {}
}:

tinypkgs.pkgs.mkShell rec {
  buildInputs = [
    tinypkgs.simulations_schedule_param
    tinypkgs.migrations_no_congestion
  ];
}